import pandas as pd
import os
from skimage.measure import label, regionprops
import numpy as np
import csv
import yaml

from handle_merge_cell import handle_merge_cell

from pathlib import Path
from openpyxl.formula import Tokenizer
with open(r'C:/Users/lamhu/Desktop/mutil-table-spreadsheet/format.yml') as file:
    params = yaml.load(file, Loader=yaml.FullLoader)

SHEET_NAME = params["SHEET_NAME"]
# TABLE_TYPE =  params["TABLE_TYPE"]
FILE_INPUT_NAME = params["FILE_INPUT_NAME"]
TABLE_ROW_HEADER_INDEX = int(params["TABLE_ROW_HEADER_INDEX"])
HEADER_INDEX_BOTTOM = int(params["HEADER_INDEX_BOTTOM"])
FILE_HANDLE = params["FILE_HANDLE"]

'''
        -----------*****-----------
You need to check setting in file 'format.yml' before run the code.
'''

#Load and check file input. unmerge and rename "Nan" values to 'merge'. save file
handle_merge_cell(FILE_INPUT_NAME)

'''
Follows code:
    - check merge cell and rename NaN value in merge group. save file
    - Parse data to dataframe.
    - handle header merge cell
    - handle body merge cell
    - write csv
'''

def extract_excel():
    '''
        Extract multiple table from one excel spreedsheet to csv file:
    '''
    #Using pandas to read excel file
    df = pd.read_excel(FILE_HANDLE, engine='openpyxl') # add: header=True --> index columns
    binary_rep = np.array(df.notnull().astype('int'))
    list_of_dataframes = []

    #Using skimage to get all table from sheet.
    l = label(binary_rep)
    for s in regionprops(l):
        item_parse = df.iloc[s.bbox[0]:s.bbox[2],s.bbox[1]:s.bbox[3]]
        list_of_dataframes.append(item_parse)
    return list_of_dataframes

def parse_data():
    '''
        table.iloc[0]: Values table row[0]
        index_start: index value of row[0]
    '''
    data_input = extract_excel()

    header_single_rows = True
    data_ouput =[]

    for table in data_input:
        headers = table.columns.str.match("merge")
        headers2 = table.columns.str.match("Unnamed")
        index_start = table.index.start
        data = []
        #Check and drop header.
        if headers[0] == True or headers2[0] == True:
            for item in table.iloc[TABLE_ROW_HEADER_INDEX]:
                if str(item) == "merge" or str(item) == "nan":
                    data_drop = table.drop(labels=index_start, axis=0)
                    data.append(data_drop)
                    break
            if len(data) == 0:
                data.append(table)
        else:
            data.append(table)

        table_col_header_index = 0
        table_col_header_bottom = table_col_header_index+1

        if len(data) == 0:
            continue
        
        for i in data[0].iloc[TABLE_ROW_HEADER_INDEX]:
            #handle header
            if table_col_header_index >0 and (str(i) == 'merge' or str(i) == 'nan'):
                '''
                solution: header 2 rows
                    header_prev: item current loop -1
                    header_current: item current loop
                    header_next: item current loop +1
                    header_bottom_current:
                    header_bottom_prev
                
                Example:
                    header =            --- somthing ---
                    seecond header:     st1    st2    st3
                after unmerged:
                    header =            header_prev{something}  header_current("merge")   header_next("merge")
                    seecond header:          st1                        st2                         st3

                Check if header merged cells ==> that mean we have the second rows header ==> delete the second rows header after handle header.
                After reaname:
                    header =            header_prev{something} + st1    header_prev{something} + st2   header_prev{something} + st3
                    seecond header:          st1                                 st2                                     st3 
                
                '''
                header_single_rows = False
                header_prev = data[0].iloc[TABLE_ROW_HEADER_INDEX][table_col_header_index-1]
                header_bottom_current = data[0].iloc[HEADER_INDEX_BOTTOM][table_col_header_index]
                header_bottom_prev = data[0].iloc[HEADER_INDEX_BOTTOM][table_col_header_index-1]
                #header current loop == 'merge'" --> set header creent loop = set header creent loop -1 + header bottom"
                header_text = header_prev + ' ' + header_bottom_current
                header_text = header_text.replace(" ", "_")
                data[0].iloc[TABLE_ROW_HEADER_INDEX][table_col_header_index]= header_text

                table_col_header_bottom = table_col_header_index+1
                #keep checking columns name == 'merge' and rename until false.
                while(table_col_header_bottom < len(data[0].iloc[TABLE_ROW_HEADER_INDEX]) and str(data[0].iloc[TABLE_ROW_HEADER_INDEX][table_col_header_bottom]) == 'merge'):
                    header_under = data[0].iloc[HEADER_INDEX_BOTTOM][table_col_header_index+1]
                    data[0].iloc[TABLE_ROW_HEADER_INDEX][table_col_header_bottom]= header_prev + ' ' + header_under
                    table_col_header_index+=1
                else:
                    # reaname the first columns in merge group
                    header_text = header_prev + ' ' + header_bottom_prev
                    header_text = header_text.replace(" ", "_")
                    data[0].iloc[TABLE_ROW_HEADER_INDEX][table_col_header_index-1]= header_text
                    table_col_header_index-=1

            table_col_header_index+=1
        # delete header bottom after reaname header
        if header_single_rows == False:
            data = data[0].drop(labels=data[0].index[HEADER_INDEX_BOTTOM], axis=0)
            data_ouput.append(data)
        else:
            data_ouput.append(data[0])
    return data_ouput

def handle_body_merge():
    '''
    Same as check header. Check value columns and rename NaN columns by the first columns in merge group
    '''
    final_data =[]
    data_ouput = parse_data()
    for data in data_ouput:
        for index in range(len(data)):
            col_index = 0
            #index rows 0: header table
            if index>0:
                data_old = []
                for item in data.iloc[index]:
                    data_old = item
                    if item == 'merge':
                        data.iloc[index][col_index] = data.iloc[index][col_index-1]
                    col_index+=1
        final_data.append(data)

    return final_data

def to_csv():
    idx = 0 
    data = handle_body_merge()
    for item in data:
        item.to_csv('data%d.csv' % (idx), header=None, index=False) #index=False --> index rows
        idx+=1
    return data

# extract_excel()
to_csv()