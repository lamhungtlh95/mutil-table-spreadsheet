import pandas as pd
import openpyxl
import csv
import yaml

with open(r'C:/Users/lamhu/Desktop/mutil-table-spreadsheet/format.yml') as file:
    params = yaml.load(file, Loader=yaml.FullLoader)

SHEET_NAME = params["SHEET_NAME"]
SHEET_HEADER_INDEX = params["SHEET_HEADER_INDEX"]
FILE_HANDLE = params["FILE_HANDLE"]


def handle_merge_cell(file_input_name):
    '''
    Check group merge file input.
    Rename value NaN to 'merge'.
    Save file.
    '''

    sheet_headers = SHEET_HEADER_INDEX
    
    wb_obj = openpyxl.load_workbook(file_input_name, data_only=True)
    sheet_ranges = wb_obj[SHEET_NAME]
    ws = wb_obj.active
    group_merged_cells = sheet_ranges.merged_cells.ranges
    range_len = range(len(group_merged_cells))
    group_cells_text = []

    #Get list merged cells
    for i in group_merged_cells:
        group_cells_text.append(i)

    for i in group_cells_text:
        get_text = str(i)
        cell_tuple = ws[get_text]
        idx =0
        try:
            ws.unmerge_cells(get_text)
        except Exception as e:
            print(e)
            continue
        
        for cell in cell_tuple[0]:
            #Normally, values after unmerged will be in the first column. We just rename Nan value.
            if idx > 0:
                cell_text = cell.coordinate
                ws[str(cell_text)] = "merge"
            idx+=1

    headers_len = len(sheet_headers)
    for row in sheet_headers:
        if row != 0:
            headers_len-=1
            ws.delete_rows(sheet_headers[headers_len], 1)
    wb_obj.save(FILE_HANDLE)